# Angular7Crud

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.


* **Angular Version - 7** 

* **Node Version - 10.15.2** 

**Note - For API refer repo https://gitlab.com/avinash.sonawane/nodeexpresscrudjwt.git**

**Steps to run :-**


* Download the repo mentioned above(link in bold note) and go to repo folder. 
* Install dependencies using "npm install" command.
* Download the current angular repo and go to repo folder.
* Install dependencies using "npm install" command.
* run the code using "ng serve" command.
* visit url "http://localhost:4200"
* regiester user by hitting register button and make login using register user.
* You can add student, edit it and delete it.

Thanks.