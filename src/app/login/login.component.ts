import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth-service.service'


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	loginForm: FormGroup;
    registerForm: FormGroup;
    submitted = false;
    submitted2 = false;
    showHidden = false;

    constructor(private formBuilder: FormBuilder, private router: Router, private authService: AuthService) { 
        if(localStorage.getItem('x-api-key') != null){
            this.router.navigateByUrl('/home');
        }
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]]
        });
        this.registerForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', [Validators.required]],
            name: ['', [Validators.required]]
        });
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    // convenience getter for easy access to form fields
    get f2() { return this.registerForm.controls; }

    onSubmit() {}

    login() {
        this.submitted = true;
        const val = this.loginForm.value;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value))

        //this.router.navigate(['/home']);

        if (val.email && val.password) {
            this.authService.login(val.email, val.password)
            .subscribe(
                // response => {
                //     console.log("Login call in error", response);
                // },
                // () => {
                //     console.log("User is logged in");
                //     this.router.navigateByUrl('/');
                // }
                data => {
                    if(data && data.status === "error"){
                        alert("Invalid Data");
                    }else{
                        localStorage.setItem('x-api-key',data.data.token);
                        this.router.navigateByUrl('/home');
                    }
                },
                error => {
                    console.log(error);
                    this.router.navigateByUrl('/');
                }
            );
        }
    }

    openRegister(){
        this.showHidden = !this.showHidden;
    }

    register() {
        this.submitted2 = true;
        const regval = this.registerForm.value;

        // stop here if form is invalid
        if (this.registerForm.invalid) {
            return;
        }

        if (regval.email && regval.password && regval.name) {
            this.authService.regUser(regval.email, regval.password, regval.name)
            .subscribe(
                data => {
                    if(data && data.status === "error"){
                        alert("Invalid Data");
                    }else{
                        alert("User Registered");
                        this.router.navigateByUrl('/');
                        this.openRegister();
                    }
                },
                error => {
                    alert("Something went wrong");
                    this.router.navigateByUrl('/');
                }
            );
        }
    }
}
