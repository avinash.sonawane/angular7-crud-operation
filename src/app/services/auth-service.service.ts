import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
	private baseUri: string = 'http://localhost:3000';
	private headers = new HttpHeaders().set('Content-Type', 'application/json');
  private tWithHeader = new HttpHeaders({ 'Content-Type': 'application/json','x-api-key': localStorage.getItem('x-api-key')});

  constructor(private http: HttpClient) { }

  login(email:string, password:string ) : any{
    return this.http.post<any>(this.baseUri + `/users/login`, {email, password}, { headers: this.headers });
  }
  regUser(email:string, password:string,name:string) : any{
    return this.http.post<any>(this.baseUri + `/users/register`, {email, password, name}, { headers: this.headers });
  }
  getStudents(page) : any{
    return this.http.get<any>(this.baseUri + `/student?page=${page}`, { headers: this.tWithHeader });
  }
  addStudent(email:string,rollno:string,name:string,phone:string,branch:string,year:string,gender:string,age:string,city:string) : any{
    return this.http.post<any>(this.baseUri + `/student`, {email,rollno,name,phone,branch,year,gender,age,city}, { headers: this.tWithHeader });
  }
  updateStudent(studId:string,email:string,rollno:string,name:string,phone:string,branch:string,year:string,gender:string,age:string,city:string) : any{
    return this.http.put<any>(this.baseUri + '/student/'+studId, {email,rollno,name,phone,branch,year,gender,age,city}, { headers: this.tWithHeader });
  }
  delStudent(studId:string){
    return this.http.delete<any>(this.baseUri + '/student/'+studId, { headers: this.tWithHeader });
  }
}
