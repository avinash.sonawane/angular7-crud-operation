import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Event, NavigationStart, NavigationEnd, NavigationError } from '@angular/router';
import { AuthService } from '../services/auth-service.service';
import{ ModalComponent }from 'angular-custom-modal'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
	studForm: FormGroup;
  mySubscription: any;
  rowid: any;
  @ViewChild('addModal') public modal: ModalComponent;
	submitted = false;
  public searchText: string;
  hideModal: boolean = false;
	studData = [];
	singleStudent = {};
  pager = {};
  pageOfItems = [];

  constructor(private formBuilder: FormBuilder,private router: Router,private authService: AuthService, private route: ActivatedRoute) {
    this.router.routeReuseStrategy.shouldReuseRoute = function () {
      return false;
    };
    this.mySubscription = this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        // Trick the Router into believing it's last link wasn't previously loaded
        this.router.navigated = false;
      }
    });
  }

  ngOnInit() {
  	this.studForm = this.formBuilder.group({
        email: ['', [Validators.required, Validators.email]],
        rollno: new FormControl('', [Validators.required]),
        name: new FormControl('', [Validators.required]),
        phone: new FormControl('', [Validators.required]),
        branch: new FormControl('', [Validators.required]),
        year: new FormControl('', [Validators.required]),
        gender: new FormControl(''),
        age: new FormControl(''),
        city: new FormControl(''),
    });
    if(localStorage.getItem('x-api-key') != null){
    	//this.getAllStudents();
      this.route.queryParams.subscribe(x => this.getAllStudents(x.page || 1));
    }else{
      this.router.navigateByUrl('/');
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.studForm.controls; }

  resetForm(){
    this.studForm.reset();
  }

  addStudent(){
  	const val = this.studForm.value;
  	this.submitted = true;

    // stop here if form is invalid
    if (this.studForm.invalid) {
        return;
    }
      this.authService.addStudent(val.email, val.rollno, val.name, val.phone, val.branch, val.year, val.gender, val.age, val.city)
      .subscribe(
        data => {
          if(data && data.status === "error"){
            alert("Session Expired Relogin please");
            document.getElementsByClassName("modal-backdrop fade in")[0].className = "";
            document.getElementsByTagName('body')[0].style.overflow = "scroll";
            this.router.navigateByUrl('/');
          }else{
              this.hideModal = true;
              document.getElementsByClassName("modal-backdrop fade in")[0].className = "";
              //document.getElementsByClassName('modal-backdrop fade in')[0].style.backgroundColor = 'red';
              document.getElementsByTagName('body')[0].style.overflow = "scroll";
              this.router.navigateByUrl('/home');
              //console.log(this.modal);
              //this.modal.close();
          }
        },
        error => {
          console.log(error);
          alert("Something got wrong while adding data");
          this.router.navigateByUrl('/home');
        }
    );
  }

  pressed(page){
    this.router.navigate(['/home'],{ queryParams: { "page":page}})
  }

  getAllStudents(page){
    this.authService.getStudents(page)
      .subscribe(
          data => {
            if(data.status == "error"){
              alert("Session Expired Relogin please");
              this.router.navigateByUrl('/');
            }else{
              //this.studData = data.pageOfItems;
              this.pager = data.pager;
              this.pageOfItems = data.pageOfItems;
            }
          },
          error => {
            window.location.reload();
            // console.log(error);
            // this.router.navigateByUrl('/');
          }
      );
  }

  openModal(stud){
    this.singleStudent = stud;
    this.rowid = stud._id;
    this.studForm.controls['rollno'].setValue(stud.rollno);
    this.studForm.controls['name'].setValue(stud.name);
    this.studForm.controls['email'].setValue(stud.email);
    this.studForm.controls['phone'].setValue(stud.phone);
    this.studForm.controls['branch'].setValue(stud.branch);
    this.studForm.controls['year'].setValue(stud.year);
    this.studForm.controls['gender'].setValue(stud.gender);
    this.studForm.controls['age'].setValue(stud.age);
    this.studForm.controls['city'].setValue(stud.city);
  }

  updateStudent(){
    const updateVal = this.studForm.value;
    this.submitted = true;

    // stop here if form is invalid
    if (this.studForm.invalid) {
        return;
    }
      this.authService.updateStudent(this.rowid,updateVal.email, updateVal.rollno, updateVal.name, updateVal.phone, updateVal.branch, updateVal.year, updateVal.gender, updateVal.age, updateVal.city)
      .subscribe(
        data => {
          if(data && data.status === "error"){
              alert("Something got wrong at db side");
          }else{
              this.hideModal = true;
              document.getElementsByClassName("modal-backdrop fade in")[0].className = "";
              //document.getElementsByClassName('modal-backdrop fade in')[0].style.backgroundColor = 'red';
              document.getElementsByTagName('body')[0].style.overflow = "scroll";
              this.router.navigateByUrl('/home');
              //console.log(this.modal);
              //this.modal.close();
          }
        },
        error => {
          console.log(error);
          alert("Something got wrong while adding data");
          this.router.navigateByUrl('/home');
        }
    );
  }

  delStudent(id, name){
    if(confirm("Are you sure you want to delete "+name)) {
      this.authService.delStudent(id)
      .subscribe(
        data =>{
          if(data && data.status === "error"){
            alert("Something got wrong at db side");
          }else{
            this.router.navigateByUrl('/home');
          }
        },
        error =>{
          alert("Error while deleting record");
          this.router.navigateByUrl('/home');
      });
    }
  }

  logout(){
    localStorage.removeItem('x-api-key');
  	this.router.navigateByUrl('/');
  }

  ngOnDestroy() {
    if (this.mySubscription) {
      this.mySubscription.unsubscribe();
    }
  }

}
